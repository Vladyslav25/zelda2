# **Zelda2**

**What did I do:**
- Lighting
- Gameplay Programming
- Engine Expertise 
- Git Support

Playthrough:

[![](http://img.youtube.com/vi/6XzcYOcgCd0/0.jpg)](https://youtu.be/6XzcYOcgCd0 "Gameplay")

I was brought in to help with this project. The group consisted only of artists who did not have much experience in the engine.
In the game, small gameplay elements were to be done by me. For example, lighting the torch and changing particles. 
I also helped the group to improve the performance of the game, for example by using material instances and combining meshes.
I also helped the group with various bugs. For example, there was a bug where the character in the game was lit incorrectly. This was due to the fact that no Lightmass Importance Volumes were set and therefore the Lightmass was not created at all. Lightmap desities were also not adjusted, resulting in a lot of light bleeding. 
The light bleeding could be solved very well by placing boxes around the buildings from the outside so that they become shadows. Some areas, such as the cellar, for example, could be positioned completely in a box, as no light from outside was needed.

After the distribution of the Lightmass Importence Volumes, Lightmass Sampels were also generated in the world. Except for the basement area.

Due to this bug, completion was delayed by several months, as no solution was found for a long time. By intervening in the config and source code, I was able to generate some blocks, but there were still some sampels missing in the basement. External contacts from the industry and the Unreal Formum were also unable to help.
<details><summary>Click for Screenshots</summary>

![](/ue4lightmass-issue01.jpg)

![](/ue4lightmass-issue02.jpg)
</details>


The solution was to position the cellar somewhere else in the world. Since the player teleports there, this could be done without any problems. This shift allowed the sampels to be generated correctly and everywhere.

----------------------------------------------------------------------------------------------------------

**Was habe ich gemacht:**
- Lighting
- Gameplay Programmierung
- Engine Expertise 
- Git Support

Playthrough:

[![](http://img.youtube.com/vi/6XzcYOcgCd0/0.jpg)](https://youtu.be/6XzcYOcgCd0 "Gameplay")

Bei diesem Projekt wurde ich als Hilfe hinzugeholt. Die Gruppe bestand nur aus Artist, welche nicht viel Erfahrung in der Engine haben.
In dem Spiel sollten kleine Gameplay Elemente von mir erledigt werden. Zum Beispiel das Fackel anzünden und der Partikel wechsel. 
Auch habe ich der Gruppe geholfen die Performence des Spiel anzuheben, indem zum Beispiel Material Instancen verwendet wurden und Meshes kombiniert wurden.
Auch bei diversen Bugs habe ich der Gruppe geholfen. So gab es den Bug, dass der Character im Spiel falsch beleuchtet wurde. Dies Lag daran, dass keine Lightmass Importance Volumes gesetzt wurden und dadurch wurde das Lightmass auch garnicht erstellt. Auch Lightmap Desities wurden nicht angepasst und somit kam es zu sehr viel Lightbleeding. 
Das Lightbleeding konnte sehr gut gelöst werden, in dem man Boxen von außen um die Gebäude stellte, damit diese Schatten werden. Einzene Bereiche, wie der Keller zum Beispiel, konnten vollständig in eine Box positioniert werden, da kein Licht von Außen von nöten war.

Nach der Verteilung der Lightmass Importence Volumes wurden auch Lightmass Sampels in der Welt generiert. Bis auf den Keller Bereich. 

Auf Grund diesem Bug hat sich die Fertigstellung um einige Monate verzögert, da lange keine Lösung gefunden wurde. Durch Eingriffe in die Config und Sourcecode konnte ich einige Blöcke generieren lassen, doch es fehlten immer noch einige Sampels im Keller. Auch konnten externe Kontakte aus der Industrie nicht weiterhelfen, sowie das Unreal Formum.
<details><summary>Click for Screenshots</summary>

![](/ue4lightmass-issue01.jpg)

![](/ue4lightmass-issue02.jpg)
</details>


Die Lösung war den Keller wo anders in der Welt zu positionieren. Da der Spieler sich in einer dort hin teleportiert, konnte dies ohne Probleme gemacht werden. Durch diese Verschiebung konnten sich die Sampels richtig und überall generieren lassen.
